import frappe
import os
from six import string_types
import json
from frappe.utils.background_jobs import enqueue

#stephan.stephan.so.update_item
@frappe.whitelist()
def update_item(bom, fields):
    if isinstance(fields, string_types):
        fields = json.loads(fields)
    set_values = ['`tabStock Entry Detail`.`{}` = `tabItem`.`{}`'.format(x, y) for x, y in fields.items()]
    frappe.db.sql("""update `tabStock Entry Detail`
    inner join tabItem on `tabStock Entry Detail`.item_code = tabItem.name
     set {} where `tabStock Entry Detail`.parent = '{}'""".format(', '.join(set_values), bom))
    return frappe.get_doc("Stock Entry", bom).as_dict()

@frappe.whitelist()
def download_table(docname,summarized=False):
    print("downloaaaaaaaaaaaaaadddddd")
    print(docname)

    # path = os.path.dirname(os.path.abspath(__file__)) +'/Items.xlsx'
    # group_by = "Group by item_code"
    group_by = ""
    short_path = ""
    if summarized:
        group_by = " Group by item_code"

        path = frappe.get_site_path() + "/private/files/stock_entry_items_sum.xlsx"
        short_path = "/private/files/stock_entry_items_sum.xlsx"
    else:
        path = frappe.get_site_path() + "/private/files/stock_entry_items.xlsx"
        short_path = "/private/files/stock_entry_items.xlsx"

    items = frappe.db.sql("""SELECT
            item_code,
            item_name,
            s_warehouse,
            t_warehouse,
            description,
            qty,
            basic_rate,
            valuation_rate,
            amount,
            uom,
            transfer_qty,
            actual_qty,
            bom_no
            FROM `tabStock Entry Detail`
            WHERE parent=%s""" + group_by,(docname),
                          as_list=True)
    print(items)


    # Writing to an excel
    # sheet using Python
    from xlsxwriter import Workbook

    # Workbook is created
    print path
    wb = Workbook(path)

    # add_sheet is used to create sheet.
    sheet1 = wb.add_worksheet()

    row = 0
    col = 0

    for i,item in enumerate([

        'item_code',
        'item_name',
        's_warehouse',
        't_warehouse',
        'description',
        'qty',
        'basic_rate',
        'valuation_rate',
        'amount',
        'uom',
        'transfer_qty',
        'actual_qty',
        'bom_no'

    ]):
        sheet1.write(row, i, item)
    row+=1
    for item in items:
        for i,_item in enumerate(item):
            sheet1.write(row,i,_item)
        row+=1

    wb.close()

    print("done writing to excel...")

    # file_url = '/private/files/summarized_supplied_items.xlsx'
    # filename, file_extension = os.path.splitext(frappe.get_site_path() + "supplied_items.xlsx")
    filename, file_extension = os.path.splitext(path)
    # dist_file = dist_folder + '/' + self.school_year + ' - ' + self.process_name + file_extension

    frappe.db.sql(""" DELETE FROM tabFile WHERE file_url=%s AND attached_to_doctype=%s""",
                  (short_path, "Purchase Order"))
    attachment_doc = frappe.get_doc({
        "doctype": "File",
        "file_name": filename,
        "file_url": short_path,
        "attached_to_name": docname,
        "attached_to_doctype": "Purchase Order",
        "old_parent": "Home/Attachments",
        "folder": "Home/Attachments",
        "is_private": 1
    })
    attachment_doc.insert()

    # success_queue(file_url)
    return {"files": [{"path": short_path, "name": filename}]}

def success_queue(file_url):

    from erpnext.setup.doctype.email_digest.quotes import get_random_quote
    quote = get_random_quote()
    frappe.publish_realtime(event='msgprint', message='Finished Generating.<br><br><i><q>{0}<br>-{1}</q></i><br><br><b><a href="{2}" class="btn btn-primary btn-block">Open File</a></b>'.format(quote[0],quote[1],file_url))



# @frappe.whitelist()
# def queue_generate_spreadsheet(docname,summarized=False):
#
#     print "QUEUEING..."
#
#     # frappe.publish_realtime(event='msgprint', message='<i>Generating your sheet now...</i><br>We will notify you once its ready')
#     # frappe.msgprint('Generating your sheet now...')
#     # spread = frappe.get_doc("Generate Spreadsheets", docname)
#
#     # if spread.status != "Processing":
#
#         # spread.status = "Processing"
#         # spread.save()
#
#     enqueue("stephan.stephan.so.download_table", queue='long',timeout='8000',docname=docname,summarized=summarized)
#     # else:
#
#         # print "Currently processing."
#         # frappe.publish_realtime(event='msgprint',
#         #                         message='Currently processing.',
#         #                         user=frappe.session.user)