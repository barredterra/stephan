import frappe
import os

from frappe.utils.background_jobs import enqueue

@frappe.whitelist()
def download_table(docname):
    print("downloaaaaaaaaaaaaaadddddd")
    print(docname)

    # path = os.path.dirname(os.path.abspath(__file__)) +'/Items.xlsx'
    path = frappe.get_site_path() + "/private/files/ordered_items.xlsx"
    items = frappe.db.sql("""SELECT item_code,
            item_name,schedule_date,qty, uom,rate,amount,
            conversion_factor,warehouse,bom
            FROM `tabPurchase Order Item` WHERE parentfield='items' AND parent=%s""",(docname),
                          as_list=True)

    # Writing to an excel
    # sheet using Python
    from xlsxwriter import Workbook

    # Workbook is created
    print path
    wb = Workbook(path)

    # add_sheet is used to create sheet.
    sheet1 = wb.add_worksheet()

    row = 0
    col = 0

    # item_code,
    # item_name, schedule_date, qty, uom, rate, amount,
    # conversion_factor, stock_uom, druckdatei_1, warehouse, bom

    for i,item in enumerate([
            'item_code','item_name','schedule_date','qty','uom','rate','amount','conversion_factor',
    'warehouse','bom']):
        sheet1.write(row, i, item)
    row+=1
    for item in items:
        for i,_item in enumerate(item):
            sheet1.write(row,i,_item)
        row+=1
    # sheet1.write(1, 0, 'ISBT DEHRADUN')
    # sheet1.write(2, 0, 'SHASTRADHARA')
    # sheet1.write(3, 0, 'CLEMEN TOWN')
    # sheet1.write(4, 0, 'RAJPUR ROAD')
    # sheet1.write(5, 0, 'CLOCK TOWER')
    # sheet1.write(0, 1, 'ISBT DEHRADUN')
    # sheet1.write(0, 2, 'SHASTRADHARA')
    # sheet1.write(0, 3, 'CLEMEN TOWN')
    # sheet1.write(0, 4, 'RAJPUR ROAD')
    # sheet1.write(0, 5, 'CLOCK TOWER')

    wb.close()

    print("done writing to excel...")

    file_url = '/private/files/ordered_items.xlsx'
    # filename, file_extension = os.path.splitext(frappe.get_site_path() + "supplied_items.xlsx")
    filename, file_extension = os.path.splitext(path)
    # dist_file = dist_folder + '/' + self.school_year + ' - ' + self.process_name + file_extension

    frappe.db.sql(""" DELETE FROM tabFile WHERE file_url=%s AND attached_to_doctype=%s""",
                  (file_url, "Purchase Order"))
    attachment_doc = frappe.get_doc({
        "doctype": "File",
        "file_name": filename,
        "file_url": file_url,
        "attached_to_name": docname,
        "attached_to_doctype": "Purchase Order",
        "old_parent": "Home/Attachments",
        "folder": "Home/Attachments",
        "is_private": 1
    })
    attachment_doc.insert()

    # success_queue(file_url)
    return {"files": [{"path":file_url,"name":filename}]}

def success_queue(file_url):

    from erpnext.setup.doctype.email_digest.quotes import get_random_quote
    quote = get_random_quote()

    frappe.publish_realtime(event='msgprint',
                            message='Finished Generating.<br><br><i><q>{0}<br>-{1}</q></i><br><br><b>'
                                    '<a href="{2}" id="downloadfile2" class="btn btn-primary btn-block">Open File</a></b>'
                                    '<script type="text/javascript">document.getElementById("downloadfile2").click(); </script>'
                            .format(quote[0],quote[1],file_url))



@frappe.whitelist()
def queue_generate_spreadsheet(docname):

    print "QUEUEING..."

    # frappe.publish_realtime(event='msgprint', message='<i>Generating your sheet now...</i><br>We will notify you once its ready')
    # frappe.msgprint('Generating your sheet now...')
    # spread = frappe.get_doc("Generate Spreadsheets", docname)

    # if spread.status != "Processing":

        # spread.status = "Processing"
        # spread.save()

    enqueue("stephan.stephan.po2.download_table", queue='long',timeout='8000',docname=docname)
    # else:

        # print "Currently processing."
        # frappe.publish_realtime(event='msgprint',
        #                         message='Currently processing.',
        #                         user=frappe.session.user)