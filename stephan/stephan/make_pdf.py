#!/usr/bin/env python
# -*- coding: utf-8 -*-

from frappe.utils.background_jobs import enqueue
import frappe,os
# from PIL import Image
from reportlab.platypus import Paragraph, Table, TableStyle, Image
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, inch,mm
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet,ParagraphStyle
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT, TA_RIGHT, TA_CENTER
from reportlab.graphics.shapes import Drawing,Line
from reportlab.pdfgen import canvas

from reportlab.lib import utils


def success_queue(file_urls):

    from erpnext.setup.doctype.email_digest.quotes import get_random_quote
    quote = get_random_quote()

    urls_str = ""

    print file_urls
    for i,url in enumerate(file_urls):
        print url
        urls_str += '<br><b><a href="{0}" id="downloadfile_{1}" target="_blank">Open File {2}</a></b>' \
                    '<script type="text/javascript">document.getElementById("downloadfile_{3}").click(); </script>'\
            .format(url['path'],str(i),url['name'],str(i))

    frappe.publish_realtime(event='msgprint', message='Finished Generating.<br><br><i><q>{0}<br>-{1}</q></i>{2}'.format(quote[0],quote[1],urls_str))


def get_image(path, width=1*mm):
    img = utils.ImageReader(path)
    iw, ih = img.getSize()
    aspect = ih / float(iw)
    return Image(path, width=width, height=(width * aspect))

class NumberedCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._codes = []
    def showPage(self):
        self._codes.append({'code': self._code, 'stack': self._codeStack})
        self._startPage()
    def save(self):
        """add page info to each page (page x of y)"""
        # reset page counter
        self._pageNumber = 0
        for code in self._codes:
            # recall saved page
            # print(self._pageNumber+1)
            self._code = code['code']
            self._codeStack = code['stack']
            self.setFont("Helvetica", 7)
            self.drawRightString(200*mm, 20*mm,
                "page %(this)i of %(total)i" % {
                   'this': self._pageNumber+1,
                   'total': len(self._codes),
                }
            )

            # data = [['', '',
            #          '',  "page %(this)i of %(total)i" % {
            #        'this': self._pageNumber+1,
            #        'total': len(self._codes),
            #     }, '', '']]
            # t_ = Table(data, [100, 110, 50, 100, 80, 50], 40)
            # t_.setStyle(TableStyle([
            #     ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            #     ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            #     ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.white),
            #     ('BOX', (0, 0), (-1, -1), 0.25, colors.white),
            # ]))
            #
            # elements.append(t_)

            canvas.Canvas.showPage(self)


def add_page_number(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Roman', 10)
    page_number_text = "%d" % (doc.page)
    canvas.drawCentredString(
        0.75 * inch,
        0.75 * inch,
        page_number_text
    )
    canvas.restoreState()


@frappe.whitelist()
def make_pdf_boms(docname,doctype):
    print "**************************************"
    print "************ MAKE BOMS PDF ***********"
    print "**************************************"
    bold_ = ParagraphStyle(name='bold', fontName='Helvetica-Bold', fontSize=14, leading=14.4,alignment=TA_RIGHT)
    bold_2 = ParagraphStyle(name='bold', fontName='Helvetica-Bold', fontSize=10.5, leading=14.4,alignment=TA_LEFT)
    styles = getSampleStyleSheet()
    styleN = styles["BodyText"]
    styleN.alignment = TA_LEFT
    styleBH = styles["Normal"]
    styleBH.alignment = TA_CENTER

    files = []

    items = []
    if doctype == "Purchase Order":
        items = frappe.db.sql("""SELECT image,item_code,item_name,schedule_date,qty,uom,rate,amount
                FROM `tabPurchase Order Item`
                              WHERE parentfield='items' AND parent=%s ORDER BY idx""",(docname),as_list=True)
    elif doctype == "BOM":
        items = frappe.db.sql("""SELECT `tabItem`.image,`tabItem`.item_code,`tabItem`.item_name
                        FROM `tabItem` INNER JOIN `tabBOM` ON `tabBOM`.item = `tabItem`.name
                                      WHERE is_default=1 AND is_default=1 AND `tabBOM`.name=%s""",
                              (docname), as_list=True)
    # print items
    # print("making pdf...")
    for item in items:

        bom_items = frappe.db.sql("""SELECT `tabBOM Item`.image,
                      `tabBOM Item`.item_code,
                      `tabBOM Item`.item_name,
                      `tabBOM Item`.qty,
                      `tabBOM Item`.uom
                            FROM `tabBOM Item`
                            INNER JOIN `tabBOM` ON `tabBOM Item`.parent=`tabBOM`.name
                          WHERE `tabBOM Item`.parentfield='items' AND `tabBOM`.item=%s
                          AND `tabBOM`.is_active=1
                          AND `tabBOM`.is_default=1 ORDER BY `tabBOM Item`.idx""",(item[1]),as_list=True)

        bom_name = frappe.db.sql("""SELECT
                      `tabBOM`.name
                            FROM `tabBOM`
                          WHERE `tabBOM`.item=%s
                          AND `tabBOM`.is_active=1
                          AND `tabBOM`.is_default=1""",(item[1]),as_list=True)
        # print(bom_name,item)
        if not bom_name:
            # frappe.throw("No BOM.")
            frappe.publish_realtime(event='msgprint',
                                    message='No BOM.')
            return

        path = frappe.get_site_path() + "/private/files/"+bom_name[0][0]+".pdf"
        # print(path)
        doc = SimpleDocTemplate(path, pagesize=letter)
        # container for the 'Flowable' objects
        elements = []
        logo = os.path.dirname(os.path.realpath(__file__))
        # elements.append(Image(logo+'/stephan.jpg',width=200,height=68))

        # data = [[Image(logo+'/stephan.jpg',100*1.5,34*1.5),'','','','','']]
        #
        # t = Table(data, [210, 90, 60, 40, 60], 10)
        # t.setStyle(TableStyle([
        #     ('ALIGN', (0, -1), (-1, -1), 'LEFT'),
        #     ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
        #     ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.white),
        #     ('BOX', (0, 0), (-1, -1), 0.25, colors.white),
        # ]))
        #
        # elements.append(t)


        data = [[Image(logo+'/stephan.jpg',100*1.5,34*1.5), '', '', '', '', Paragraph('STÜCKLISTE',bold_)],
                ['', '', '', '', '', Paragraph(bom_name[0][0],bold_)]]

        t_ = Table(data, [140, 10, 10, 10, 180], 30)
        t_.setStyle(TableStyle([
            ('ALIGN', (0, 0), (-1, -1), 'RIGHT'),
            ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.white),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.white),
        ]))



        elements.append(t_)

        d = Drawing(540, 1)
        d.add(Line(-40, 0, 500, 0))
        elements.append(d)

        item_name = Paragraph(item[2], styleN)
        _img = frappe.db.sql("""SELECT image FROM `tabItem` WHERE name=%s""",(item[1]))
        print _img
        if _img:
            if _img[0][0]:
                #get_image(frappe.get_site_path() + "/public/"+itemb[0],width=20*mm)
                # data = [['', '', '', '', '', Image(frappe.get_site_path() +_img[0][0],3 * inch,3* inch)]]
                try:
                    data = [[Paragraph('Artikelname', bold_2), item_name,
                             '', '', '', get_image(frappe.get_site_path() +_img[0][0],width=3 * inch)]]
                    # data = [['', '', '', '', '', get_image(frappe.get_site_path() +_img[0][0],width=3 * inch)]]
                except:
                    data = [[Paragraph('Artikelname', bold_2), item_name,
                             '', '', '', get_image(frappe.get_site_path() +'/public'+ _img[0][0], width=3 * inch)]]
                    # data = [['', '', '', '', '', get_image(frappe.get_site_path() +'/public'+ _img[0][0], width=3 * inch)]]
                # t_ = Table(data, [200, 100, 100, 10, 10, 400], 200)
                # t_.setStyle(TableStyle([
                #     ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                #     ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                #     ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.white),
                #     ('BOX', (0, 0), (-1, -1), 0.25, colors.white),
                # ]))
                #
                # elements.append(t_)
            else:
                data = [[Paragraph('Artikelname', bold_2), item_name,
                         '', '', '', '']]
        else:
            data = [[Paragraph('Artikelname', bold_2), item_name,
                     '', '', '', '']]


        # data = [[Paragraph('Artikelname',bold_2),item_name,
        #          '','', '', '']]
        t_ = Table(data, [100, 160, 10, 10, 10,250], 40)
        t_.setStyle(TableStyle([
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.transparent),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.transparent),
        ]))

        elements.append(t_)

        data = [[Paragraph('Artikel',bold_2), item[1], '', '', '', '']]
        t_ = Table(data, [110, 220, 10, 10, 10, 180], 40)
        t_.setStyle(TableStyle([
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.transparent),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.transparent),
        ]))

        elements.append(t_)

        data = [[Paragraph('Date of Creation', bold_2), '17-12-2018', '', '', '', '']]
        t_ = Table(data, [110, 220, 10, 10, 10, 180], 40)
        t_.setStyle(TableStyle([
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.transparent),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.transparent),
        ]))

        elements.append(t_)

        data = [['', '', '', '', '', '']]
        t_ = Table(data, [110, 220, 10, 10, 10, 180], 50)
        t_.setStyle(TableStyle([
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.transparent),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.transparent),
        ]))

        elements.append(t_)

        # print("x-x-x-x-x-x-x-x-x-x-x-x")
        # print(item[1])

        # _img = frappe.db.sql("""SELECT image FROM `tabItem` WHERE name=%s""",(item[1]))
        # print _img
        # if _img:
        #     if _img[0][0]:
        #         #get_image(frappe.get_site_path() + "/public/"+itemb[0],width=20*mm)
        #         # data = [['', '', '', '', '', Image(frappe.get_site_path() +_img[0][0],3 * inch,3* inch)]]
        #         try:
        #             data = [['', '', '', '', '', get_image(frappe.get_site_path() +_img[0][0],width=3 * inch)]]
        #         except:
        #             data = [['', '', '', '', '', get_image(frappe.get_site_path() +'/public'+ _img[0][0], width=3 * inch)]]
        #         t_ = Table(data, [200, 100, 100, 10, 10, 400], 200)
        #         t_.setStyle(TableStyle([
        #             ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
        #             ('VALIGN', (0, 0), (-1, -1), 'TOP'),
        #             ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.white),
        #             ('BOX', (0, 0), (-1, -1), 0.25, colors.white),
        #         ]))
        #
        #         elements.append(t_)



        # data = [['', '',
        #          '', Paragraph('Date of Creation', bold_2), '17-12-2018', '']]
        # t_ = Table(data, [100, 110, 50, 100, 80, 50], 40)
        # t_.setStyle(TableStyle([
        #     ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
        #     ('VALIGN', (0, 0), (-1, -1), 'TOP'),
        #     ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.white),
        #     ('BOX', (0, 0), (-1, -1), 0.25, colors.white),
        # ]))
        #
        # elements.append(t_)


        data = []
        for i,itemb in enumerate(bom_items):
            # try:
            # print(1,itemb[1])
            # print(0,itemb[0])
            if itemb[0]:
                if os.path.exists(frappe.get_site_path() + "/public/"+itemb[0]):
                    if ".jpg" in itemb[0] or ".png" in itemb[0] or ".JPG" in itemb[0] or ".PNG" in itemb[0]\
                            or ".JPEG" in item[0] or ".jpeg" in itemb[0]:
                        # a = Image(frappe.get_site_path() + "/public/"+itemb[0],0.5 * inch,0.5* inch)
                        a = get_image(frappe.get_site_path() + "/public/"+itemb[0],width=20*mm)
                        # a.drawHeight = 2 * inch
                        # a.drawWidth = 2 * inch
                        itemb[0] = a

                        print("successfully opened image",item[1])
            else:
                itemb[0] = ""

            # print(itemb[3])
            itemb[3] = int(itemb[3])

            itemb[2] = Paragraph(itemb[2], styleN)

            itemb.insert(0,i+1)
            # except Exception as e:
            #     print(e)
        data += bom_items
        # t = Table(data, 5 * [0.4 * inch], 4 * [0.4 * inch])

        t = Table([[Paragraph('Sr',bold_2),Paragraph('Bild',bold_2),Paragraph('Artikelabkürzung',bold_2),
                 Paragraph('Artikelname',bold_2),Paragraph('Menge',bold_2),Paragraph('UOM',bold_2)]],
                  [30, 65, 105, 240, 50, 40], 40)
        t.setStyle(TableStyle([
            ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
            ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ]))

        elements.append(t)

        t = Table(data, [30,65,105,240,50,40], 85)
        t.setStyle(TableStyle([
                               ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                               ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                               ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                               ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                               ]))

        elements.append(t)
        # write the document to disk
        # doc.build(elements, canvasmaker=NumberedCanvas)
        doc.build(elements,
                  onFirstPage=add_page_number,
                  onLaterPages=add_page_number
                  )

        file_url = '/private/files/'+bom_name[0][0]+'.pdf'
        # filename, file_extension = os.path.splitext(frappe.get_site_path() + "supplied_items.xlsx")
        filename, file_extension = os.path.splitext(path)
        # dist_file = dist_folder + '/' + self.school_year + ' - ' + self.process_name + file_extension

        frappe.db.sql(""" DELETE FROM tabFile WHERE file_url=%s AND attached_to_doctype=%s""",
                      (file_url, doctype))
        attachment_doc = frappe.get_doc({
            "doctype": "File",
            "file_name": filename,
            "file_url": file_url,
            "attached_to_name": docname,
            "attached_to_doctype": doctype,
            "old_parent": "Home/Attachments",
            "folder": "Home/Attachments",
            "is_private": 1
        })
        attachment_doc.insert()
        # break
        files.append({"path":file_url,"name":filename})


    # success_queue(files)
    # frappe.publish_realtime(event='msgprint',
    #                         message='Done generating BOMS pdf.')
    print("DONE!")
    return {"files":files}


@frappe.whitelist()
def make_pdf(docname):
    items = frappe.db.sql("""SELECT image,item_code,item_name,schedule_date,qty,uom,rate,amount FROM `tabPurchase Order Item`
                          WHERE parentfield='items' AND parent=%s""",(docname),as_list=True)
    print items
    print("making pdf...")
    from reportlab.lib import colors
    from reportlab.lib.pagesizes import letter, inch
    from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
    path = frappe.get_site_path() + "/private/files/"+docname+".pdf"
    print(path)
    doc = SimpleDocTemplate(path, pagesize=letter)
    # container for the 'Flowable' objects
    elements = []

    # data = [['00', '01', '02', '03', '04'],
    #         ['10', '11', '12', '13', '14'],
    #         ['20', '21', '22', '23', '24'],
    #         ['30', '31', '32', '33', '34']]
    data = [[Paragraph('Image',ParagraphStyle),Paragraph('Artikelabkürzung',ParagraphStyle),Paragraph('Item Name',ParagraphStyle),
             Paragraph('Schedule Date',ParagraphStyle),Paragraph('Qty',ParagraphStyle),
             Paragraph('UOM',ParagraphStyle),Paragraph('Rate',ParagraphStyle),Paragraph('Amount',ParagraphStyle)]]
    for item in items:
        # try:
        if os.path.exists(frappe.get_site_path() + "/public/"+item[0]):
            if ".jpg" in item[0] or ".png" in item[0]:
                a = Image(frappe.get_site_path() + "/public/"+item[0],0.5 * inch,0.5* inch)
                # a.drawHeight = 2 * inch
                # a.drawWidth = 2 * inch
                item[0] = a
                print("successfully opened image",item[1])
        else:
            item[0] = ""
        # except Exception as e:
        #     print(e)
    data += items
    # t = Table(data, 5 * [0.4 * inch], 4 * [0.4 * inch])
    t = Table(data, [60,70,100,70,60,30,70,70], 50)
    t.setStyle(TableStyle([
                           ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                           ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                           ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                           ]))

    elements.append(t)
    # write the document to disk
    doc.build(elements)

    file_url = '/private/files/'+docname+'.pdf'
    # filename, file_extension = os.path.splitext(frappe.get_site_path() + "supplied_items.xlsx")
    filename, file_extension = os.path.splitext(path)
    # dist_file = dist_folder + '/' + self.school_year + ' - ' + self.process_name + file_extension

    frappe.db.sql(""" DELETE FROM tabFile WHERE file_url=%s AND attached_to_doctype=%s""",
                  (file_url, "Purchase Order"))
    attachment_doc = frappe.get_doc({
        "doctype": "File",
        "file_name": filename,
        "file_url": file_url,
        "attached_to_name": docname,
        "attached_to_doctype": "Purchase Order",
        "old_parent": "Home/Attachments",
        "folder": "Home/Attachments",
        "is_private": 1
    })
    attachment_doc.insert()



@frappe.whitelist()
def queue_generate_spreadsheet(docname,doctype):

    print "QUEUEING..."

    enqueue("stephan.stephan.make_pdf.make_pdf_boms", queue='long',timeout='8000',
            docname=docname,doctype=doctype)