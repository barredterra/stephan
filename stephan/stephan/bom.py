import frappe
import os
from frappe.utils import cint, cstr, flt
from erpnext.stock.get_item_details import get_price_list_rate
from frappe import _

@frappe.whitelist()
def get_valuation_rate(self, args):
    """ Get weighted average of valuation rate from all warehouses """
    valuation_rate = frappe.db.get_value("Item", args['item_code'], "valuation_rate")
    print "####################################"
    print "####################################"
    print "####################################"
    print "####################################"
    print valuation_rate,args['item_code']


    return valuation_rate

from frappe.utils.background_jobs import enqueue
@frappe.whitelist()
def update_cost_(docname, update_parent=True, from_child_bom=False, save=True):
    enqueue("stephan.stephan.bom.update_cost", queue='long',timeout='8000',docname=docname,
            update_parent=update_parent,from_child_bom=from_child_bom,save=save)

@frappe.whitelist()
def update_cost(docname, update_parent=True, from_child_bom=False, save=True):

    self = frappe.get_doc("BOM",docname)

    if self.docstatus == 2:
        return

    existing_bom_cost = self.total_cost

    for d in self.get("items"):
        rate = get_rm_rate(self,{
            "item_code": d.item_code,
            "bom_no": d.bom_no,
            "qty": d.qty,
            "uom": d.uom,
            "stock_uom": d.stock_uom,
            "conversion_factor": d.conversion_factor
        })
        if rate:
            print "Rate",rate
            d.rate = rate
        d.amount = flt(d.rate) * flt(d.qty)

    if self.docstatus == 1:
        self.flags.ignore_validate_update_after_submit = True
        self.calculate_cost()
    if save:
        self.save()
    self.update_exploded_items()

    # update parent BOMs
    if self.total_cost != existing_bom_cost and update_parent:
        parent_boms = frappe.db.sql_list("""select distinct parent from `tabBOM Item`
            where bom_no = %s and docstatus=1 and parenttype='BOM'""", self.name)

        for bom in parent_boms:
            frappe.get_doc("BOM", bom).update_cost(from_child_bom=True)

    if not from_child_bom:
        frappe.msgprint(_("Cost Updated"))

@frappe.whitelist()
def get_rm_rate(self, arg):
    """	Get raw material rate as per selected method, if bom exists takes bom cost """
    rate = 0
    if not self.rm_cost_as_per:
        self.rm_cost_as_per = "Valuation Rate"

    # if arg.get('scrap_items'):
    #     rate = get_valuation_rate(arg)
    if arg:
        # if arg.get('bom_no') and self.set_rate_of_sub_assembly_item_based_on_bom:
        #     rate = self.get_bom_unitcost(arg['bom_no'])
        # else:
        # if self.rm_cost_as_per == 'Valuation Rate':
        rate = get_valuation_rate(self,arg)
    return flt(rate)



from frappe.utils.background_jobs import enqueue

@frappe.whitelist()
def download_table(docname):
    print("downloaaaaaaaaaaaaaadddddd")
    print(docname)

    # path = os.path.dirname(os.path.abspath(__file__)) +'/Items.xlsx'
    path = frappe.get_site_path() + "/private/files/bom_items.xlsx"
    items = frappe.db.sql("""SELECT item_code,item_name,description,qty,
uom,stock_qty, stock_uom,conversion_factor,amount
            FROM `tabBOM Item` WHERE parentfield='items' AND parent=%s""",(docname),
                          as_list=True)

    # Writing to an excel
    # sheet using Python
    from xlsxwriter import Workbook

    # Workbook is created
    print path
    wb = Workbook(path)

    # add_sheet is used to create sheet.
    sheet1 = wb.add_worksheet()

    row = 0
    col = 0

    for i,item in enumerate([
        'item_code', 'item_name', 'description', 'qty',
        'uom', 'stock_qty', 'stock_uom', 'conversion_factor', 'amount'
]):
        sheet1.write(row, i, item)
    row+=1
    for item in items:
        for i,_item in enumerate(item):
            sheet1.write(row,i,_item)
        row+=1

    wb.close()

    print("done writing to excel...")

    file_url = '/private/files/bom_items.xlsx'
    # filename, file_extension = os.path.splitext(frappe.get_site_path() + "supplied_items.xlsx")
    filename, file_extension = os.path.splitext(path)
    # dist_file = dist_folder + '/' + self.school_year + ' - ' + self.process_name + file_extension

    frappe.db.sql(""" DELETE FROM tabFile WHERE file_url=%s AND attached_to_doctype=%s""",
                  (file_url, "BOM"))
    attachment_doc = frappe.get_doc({
        "doctype": "File",
        "file_name": filename,
        "file_url": file_url,
        "attached_to_name": docname,
        "attached_to_doctype": "BOM",
        "old_parent": "Home/Attachments",
        "folder": "Home/Attachments",
        "is_private": 1
    })
    attachment_doc.insert()

    # success_queue(file_url)
    print file_url
    print filename
    return {"files": [{"path": file_url, "name": filename}]}

def success_queue(file_url):

    from erpnext.setup.doctype.email_digest.quotes import get_random_quote
    quote = get_random_quote()
    frappe.publish_realtime(event='msgprint', message='Finished Generating.<br><br><i><q>{0}<br>-{1}</q></i><br><br><b>'
                                                      '<a href="{2}" class="btn btn-primary btn-block">Open File</a></b>'.format(quote[0],quote[1],file_url))



@frappe.whitelist()
def queue_generate_spreadsheet(docname):

    print "QUEUEING..."

    # frappe.publish_realtime(event='msgprint', message='<i>Generating your sheet now...</i><br>We will notify you once its ready')
    # frappe.msgprint('Generating your sheet now...')
    # spread = frappe.get_doc("Generate Spreadsheets", docname)

    # if spread.status != "Processing":

        # spread.status = "Processing"
        # spread.save()

    enqueue("stephan.stephan.bom.download_table", queue='long',timeout='8000',docname=docname)
    # else:

        # print "Currently processing."
        # frappe.publish_realtime(event='msgprint',
        #                         message='Currently processing.',
        #                         user=frappe.session.user)