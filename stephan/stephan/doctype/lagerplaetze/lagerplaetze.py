# -*- coding: utf-8 -*-
# Copyright (c) 2019, Mainul Islam and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document


class Lagerplaetze(Document):
    def validate(self):
        for item in self.lagerplatznr:
            item_doc = frappe.get_doc("Item",item.item)
            if item_doc.lagerplatz:
                if self.name not in item_doc.lagerplatz:
                    item_doc.lagerplatz += ", " + self.name
            else:
                item_doc.lagerplatz = self.name

            item_doc.save()