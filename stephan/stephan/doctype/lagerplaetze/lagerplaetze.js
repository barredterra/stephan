// Copyright (c) 2019, Mainul Islam and contributors
// For license information, please see license.txt

frappe.provide("erpnext.buying");

{% include 'erpnext/public/js/controllers/buying.js' %};

frappe.ui.form.on('Lagerplaetze', {
	refresh: function(frm) {
		frm.fields_dict.lagerplatznr.grid.get_field('item').get_query = function() {
			return erpnext.queries.item({is_stock_item: 1});
		};
	},
	scan_barcode: function() {
		let transaction_controller= new erpnext.TransactionController({frm:this.frm});
		transaction_controller.scan_barcode();
	},
		barcode: function(doc, cdt, cdn) {
		var d = locals[cdt][cdn];
		if (d.barcode) {
			frappe.call({
				method: "erpnext.stock.get_item_details.get_item_code",
				args: {"barcode": d.barcode },
				callback: function(r) {
					if (!r.exe){
						frappe.model.set_value(cdt, cdn, "item_code", r.message);
					}
				}
			});
		}
	},
});
