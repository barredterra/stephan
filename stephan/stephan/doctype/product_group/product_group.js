// Copyright (c) 2018, Mainul Islam and contributors
// For license information, please see license.txt

frappe.ui.form.on('Product Group', {
	onload: function(frm) {
		frm.list_route = "Tree/Product Group";

		//get query select Product Group
		frm.fields_dict['parent_product_group'].get_query = function(doc,cdt,cdn) {
			return{
				filters:[
					['Product Group', 'is_group', '=', 1],
					['Product Group', 'name', '!=', doc.product_group_name]
				]
			}
		}
	},

	refresh: function(frm) {
		frm.trigger("set_root_readonly");
		frm.add_custom_button(__("Product Group Tree"), function() {
			frappe.set_route("Tree", "Product Group");
		});

		if(!frm.is_new()) {
			frm.add_custom_button(__("Items"), function() {
				frappe.set_route("List", "Item", {"product_group": frm.doc.name});
			});
		}
	},

	set_root_readonly: function(frm) {
		// read-only for root Product Group
		frm.set_intro("");
		if(!frm.doc.parent_product_group) {
			frm.set_read_only();
			frm.set_intro(__("This is a root Product Group and cannot be edited."), true);
		}
	},

	page_name: frappe.utils.warn_page_name_change
});
