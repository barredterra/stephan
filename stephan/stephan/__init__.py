import frappe

#bench execute stephan.stephan.set_se_stat
def set_se_stat():
    for se in frappe.db.sql("""select name from `tabStock Entry`"""):
        print(se[0])
        se_doc = frappe.get_doc("Stock Entry",se[0])
        if se_doc.docstatus == 0:
            se_doc.status = "Draft"
            frappe.db.sql("""update `tabStock Entry` set status=%s where name=%s""",('Draft',se[0]))
        elif se_doc.docstatus == 1:
            se_doc.status = "Submitted"
            frappe.db.sql("""update `tabStock Entry` set status=%s where name=%s""", ('Submitted', se[0]))
        elif se_doc.docstatus == 2:
            se_doc.status = "Cancelled"
            frappe.db.sql("""update `tabStock Entry` set status=%s where name=%s""", ('Cancelled', se[0]))
        # se_doc.save()
        frappe.db.commit()