import frappe

def after_install():
    make_product_group_root()


def make_product_group_root():

    if not frappe.db.exists("Product Group", "All Product Group"):
        frappe.get_doc({
            "doctype": "Product Group",
            "product_group_name": "All Product Group",
            "is_group": 1,
            "lft": 1,
            "rgt": 2
        }).insert()