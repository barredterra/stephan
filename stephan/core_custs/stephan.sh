#!/usr/bin/env bash

root_path="/home/mainul/erp"
cp $root_path/apps/stephan/stephan/core_custs/frappe/tools.js $root_path/apps/frappe/frappe/public/js/frappe/misc/tools.js

cd &&
cd erp &&
bench build &&
bench clear-cache &&
bench restart &&
echo "Success!"

#. /home/mainul/erp/apps/stephan/stephan/core_custs/stephan.sh