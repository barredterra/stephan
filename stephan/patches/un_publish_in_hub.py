import frappe

def execute():
    frappe.reload_doctype('Item', True)
    frappe.db.sql("""update tabItem set publish_in_hub=0 where publish_in_hub=1""")