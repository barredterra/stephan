import frappe
from stephan.install import make_product_group_root

def execute():
    frappe.reload_doctype("Product Group", True)
    make_product_group_root()
    docs = frappe.get_all("Product Group", {"parent_product_group": "", "name": ("!=", "All Product Group")})
    for d in docs:
        gdoc = frappe.get_doc("Product Group", d.name)
        if not gdoc.product_group_name:
            gdoc.set('product_group_name', gdoc.name)
        gdoc.set('parent_product_group', 'All Product Group')
        gdoc.lft = ''
        gdoc.rgt = ''
        gdoc.save()