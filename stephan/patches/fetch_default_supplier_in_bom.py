import frappe

def execute():
    frappe.reload_doctype("BOM", True)
    frappe.db.sql("""update tabBOM inner join tabItem on tabItem.name = tabBOM.item set tabBOM.default_supplier = tabItem.default_supplier""")
    frappe.db.commit()