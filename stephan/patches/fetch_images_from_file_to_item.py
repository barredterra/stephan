import frappe

def execute():
    frappe.reload_doctype("Item", True)
    frappe.db.sql("""update tabItem inner join tabFile on tabItem.name = tabFile.attached_to_name and tabFile.attached_to_doctype = "Item" 
    set tabItem.image = tabFile.file_url""")
    frappe.db.commit()