import frappe

def execute():
    frappe.reload_doctype("BOM", True)
    frappe.db.sql("""update tabBOM inner join tabItem on tabItem.name = tabBOM.item set tabBOM.barcode = tabItem.barcode, tabBOM.product_group = tabItem.product_group""")
    frappe.db.commit()