import frappe

def execute():
    frappe.reload_doctype("BOM", True)
    frappe.db.sql("""update tabBOM inner join tabItem on tabItem.name = tabBOM.item set tabBOM.item_group = tabItem.item_group, tabBOM.sortiment = tabItem.sortiment where tabBOM.item is not null""")
    frappe.db.commit()