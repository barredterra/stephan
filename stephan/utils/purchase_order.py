import frappe
from frappe.model.mapper import get_mapped_doc
from frappe.utils import flt


@frappe.whitelist()
def make_manual_stock_entry(source_name, target_doc=None):
	def postprocess(source, target):
		target.purpose = "Repack"
		target.purchase_order = source.name
		

	doc = get_mapped_doc("Purchase Order", source_name,	{
		"Purchase Order": {
			"doctype": "Stock Entry",
			"validation": {
				"docstatus": ["=", 1]
			}
		},
		"Purchase Order Item": {
			"doctype": "Stock Entry Detail",
			"field_map": {
				"warehouse": "t_warehouse"
			}
		},
		"Purchase Order Item Supplied": {
			"doctype": "Stock Entry Detail",
			"field_map": {
				"main_item_code": "item_code",
				"rm_item_name": "item_name",
				"reserve_warehouse": "s_warehouse",
				"reserve_warehouse": "s_warehouse",
				"stock_uom": "uom",
				"required_qty": "qty"
			}
		}
	}, target_doc, postprocess)

	return doc

def validate(doc, method):
	if doc.is_subcontracted=="Yes":
		fields_map = {
			'rm_item_name': 'item_name',
			'druckdatei_1': 'druckdatei1',
			'druckdatei_1_google_drive': 'druckdatei_1_google_drive',
			'druckdatei_2': 'druckdatei_2',
			'druckdatei_2_google_drive': 'druckdatei_2_google_drive',
			'schnittdatei': 'schnittdatei',
			'schnittdatei_google_drive': 'schnittdatei_google_drive',
			'material': 'material',
			'zuschnitt': 'zuschnitt',
			'verpackung': 'verpackung',
			'farbdruck': 'farbdruck',
			'bestellfaktor': 'bestellfaktor',
			'kontrolliert': 'kontrolliert',
			'datum_kontrolle': 'datum_kontrolle',
			'kommentar_aenderung': 'kommentar_aenderung',
		}
		for item in doc.supplied_items or []:
			item_doc = frappe.get_doc("Item", item.rm_item_code)
			for key, val in fields_map.items():
				item.set(key, item_doc.get(val))
			if flt(item.bestellfaktor):
				item.set('bestellmenge_berechnet', flt(item.required_qty) * flt(item.bestellfaktor))
			else:
				item.set('bestellmenge_berechnet', item.required_qty)
			if not item.reserve_warehouse and doc.default_reserve_warehouse:
				item.set('reserve_warehouse', doc.default_reserve_warehouse)
