from stephan.utils.production_planning_tool import _get_production_items
from erpnext.manufacturing.doctype.production_plan.production_plan import ProductionPlan


def overwrite_methods_of_reports():
	from stephan.utils.reports.projected_qty import get_data
	from stephan.utils.reports.stock_balance import _get_columns, get_items
	from stephan.utils.reports.stock_ledger import stock_ledger_execute
	import erpnext.stock.report.stock_projected_qty.stock_projected_qty
	import erpnext.stock.report.stock_balance.stock_balance
	import erpnext.stock.report.stock_ledger.stock_ledger

	erpnext.stock.report.stock_projected_qty.stock_projected_qty.get_data = get_data
	erpnext.stock.report.stock_balance.stock_balance.get_columns = _get_columns
	erpnext.stock.report.stock_balance.stock_balance.get_items = get_items
	erpnext.stock.report.stock_ledger.stock_ledger.execute = stock_ledger_execute


def get_boot_info(bootinfo):
	ProductionPlan.get_production_items = _get_production_items
	overwrite_methods_of_reports()