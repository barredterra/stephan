import frappe
from six import string_types
import json


@frappe.whitelist()
def update_item(bom, fields):
    if isinstance(fields, string_types):
        fields = json.loads(fields)
    set_values = ['`tabBOM Item`.`{}` = `tabItem`.`{}`'.format(x, y) for x, y in fields.items()]
    frappe.db.sql("""update `tabBOM Item` inner join tabItem on `tabBOM Item`.item_code = tabItem.name
     set {} where `tabBOM Item`.parent = '{}'""".format(', '.join(set_values), bom))
    return frappe.get_doc("BOM", bom).as_dict()



@frappe.whitelist()
def update_item_po(bom, fields):
    if isinstance(fields, string_types):
        fields = json.loads(fields)
    set_values = ['`tabPurchase Order Item`.`{}` = `tabItem`.`{}`'.format(x, y) for x, y in fields.items()]
    frappe.db.sql("""update `tabPurchase Order Item` inner join tabItem on `tabPurchase Order Item`.item_code = tabItem.name
     set {} where `tabPurchase Order Item`.parent = '{}'""".format(', '.join(set_values), bom))
    return frappe.get_doc("Purchase Order", bom).as_dict()
