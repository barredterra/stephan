import frappe
import os
from frappe.modules import get_doc_path
from frappe import _

def _get_print_format(doctype, print_format):
    if print_format.disabled:
        frappe.throw(_("Print Format {0} is disabled").format(print_format.name),
            frappe.DoesNotExistError)

    # server, find template
    module = print_format.module or frappe.db.get_value("DocType", doctype, "module") 
    path = os.path.join(get_doc_path(module, "Print Format", print_format.name), frappe.scrub(print_format.name) + ".html")

    if os.path.exists(path):
        with open(path, "r") as pffile:
            return pffile.read()
    else:
        if print_format.html:
            return print_format.html
        else:
            frappe.throw(_("No template found at path: {0}").format(path),
                frappe.TemplateNotFoundError)
