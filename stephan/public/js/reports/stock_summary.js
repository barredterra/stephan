const filters = [
    {
        fieldname: "item_name",
        fieldtype: "Data",
        label: __("Item Name")
    },
    {
        fieldname: "default_supplier",
        fieldtype: "Link",
        options: "Supplier",
        label: __("Default Supplier")
    },
    {
        fieldname: "product_group",
        fieldtype: "Link",
        options: "Product Group",
        label: __("Product Group")
    },
    {
        fieldname: "barcode",
        fieldtype: "Data",
        label: __("Barcode")
    },
    {
        fieldname: "sortiment",
        fieldtype: "Link",
        options: "Sortiment",
        label: __("Sortiment")
    },
    {
        fieldname: "asin",
        fieldtype: "Data",
        label: __("Asin")
    },
    {
        fieldname: "topseller_kategorie",
        fieldtype: "Link",
        options: "Topseller Kategorie",
        label: __("Topseller Kategorie")
    },
    {
        fieldname: "project",
        fieldtype: "Link",
        options: "Project",
        label: __("Project")
    }
];

frappe.pages['stock-balance'].on_page_load = function(old) {
    function update_function(wrapper) {
        old(wrapper);
        let page = wrapper.page;
        filters.forEach(f => {
            f['change'] = function() {
                page.item_dashboard.start = 0;
                page.item_dashboard.refresh();
            }
            page[f.fieldname] = page.add_field(f);
        });
        $(page.sort_selector.wrapper).appendTo($(page.sort_selector.wrapper).parent())
        let check_dashboard = setInterval(function(){
            if (typeof page.item_dashboard === "undefined")
                return;
            page.item_dashboard.before_refresh = function() {
                this.item_code = page.item_field.get_value();
                this.warehouse = page.warehouse_field.get_value();
                this.item_group = page.item_group_field.get_value();
                this.filters = {}
                filters.forEach(f => {
                    if(page[f.fieldname].get_value())
                        this.filters[f.fieldname] = page[f.fieldname].get_value();
                })
            }
            page.item_dashboard.refresh = function() {
                if(this.before_refresh) {
                    this.before_refresh();
                }
        
                var me = this;
                frappe.call({
                    method: 'stephan.utils.reports.stock_balance.get_data',
                    args: {
                        item_code: this.item_code,
                        warehouse: this.warehouse,
                        item_group: this.item_group,
                        start: this.start,
                        sort_by: this.sort_by,
                        sort_order: this.sort_order,
                        filters: this.filters
                    },
                    callback: function(r) {
                        me.render(r.message);
                    }
                });
            }
            clearInterval(check_dashboard)
        })
    }
    return update_function
}(frappe.pages['stock-balance'].on_page_load)