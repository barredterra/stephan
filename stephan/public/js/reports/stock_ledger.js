
/**
 * Created by mainul on 6/12/18.
 */
frappe.query_reports['Stock Ledger'].filters[0].hidden=true;
frappe.query_reports['Stock Ledger'].filters.splice(7,2);
let filters = [
    {
        fieldname: "item_name",
        fieldtype: "Data",
        label: __("Item Name")
    },
    {
        fieldname: "default_supplier",
        fieldtype: "Link",
        options: "Supplier",
        label: __("Default Supplier")
    },
    {
        fieldname: "product_group",
        fieldtype: "Link",
        options: "Product Group",
        label: __("Product Group")
    },
    {
        fieldname: "barcode",
        fieldtype: "Data",
        label: __("Barcode")
    },
    {
        fieldname: "sortiment",
        fieldtype: "Link",
        options: "Sortiment",
        label: __("Sortiment")
    },
    {
        fieldname: "asin",
        fieldtype: "Data",
        label: __("Asin")
    },
    {
        fieldname: "topseller_kategorie",
        fieldtype: "Link",
        options: "Topseller Kategorie",
        label: __("Topseller Kategorie")
    }
]
filters.forEach((f, i) => {
    frappe.query_reports['Stock Ledger'].filters.splice(i + 6, 0, f)
})