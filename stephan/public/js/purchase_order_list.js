frappe.listview_settings['Purchase Order'] = {
    	get_indicator: function (doc) {
		if (doc.status === "Closed") {
			return [__("Closed"), "green", "status,=,Closed"];
		} else if (doc.status === "Delivered") {
			return [__("Delivered"), "green", "status,=,Closed"];
		} else if (flt(doc.per_received, 2) < 100 && doc.status !== "Closed") {
			if (flt(doc.per_billed, 2) < 100) {
				return [__("To Receive and Bill"), "orange",
					"per_received,<,100|per_billed,<,100|status,!=,Closed"];
			} else {
				return [__("To Receive"), "orange",
					"per_received,<,100|per_billed,=,100|status,!=,Closed"];
			}
		} else if (flt(doc.per_received, 2) == 100 && flt(doc.per_billed, 2) < 100 && doc.status !== "Closed") {
			return [__("To Bill"), "orange", "per_received,=,100|per_billed,<,100|status,!=,Closed"];
		} else if (flt(doc.per_received, 2) == 100 && flt(doc.per_billed, 2) == 100 && doc.status !== "Closed") {
			return [__("Completed"), "green", "per_received,=,100|per_billed,=,100|status,!=,Closed"];
		}
	},

    onload: function (list) {



         list.page.add_menu_item(__("Update Images"), function() {
			// console.log(list.get_checked_items(true));
            var items = list.get_checked_items(true);
            for(var i=0;i<items.length;i++) {
                  frappe.call({
                        method: "stephan.utils.bom.update_item_po",
                        args: {
                            bom: items[i],
                            fields: {'image':'image'}
                        },
                        freeze: true,
                        callback: r => {
                            if(r['message']){
                                console.log("done update image.");
                            }
                        }
                    })
            }
		});

              list.page.add_menu_item(__("Update Names"), function() {
			// console.log(list.get_checked_items(true));
            var items = list.get_checked_items(true);
            for(var i=0;i<items.length;i++) {
                  frappe.call({
                        method: "stephan.utils.bom.update_item_po",
                        args: {
                            bom: items[i],
                            fields: {'item_name':'item_name'}
                        },
                        freeze: true,
                        callback: r => {
                            if(r['message']){
                                console.log("done update item name.");
                            }
                        }
                    })
            }
		});


    }
};