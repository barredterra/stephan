frappe.provide('stephan')
//erpnext.utils.copy_value_in_all_row
stephan.ProductionPlaningController = frappe.ui.form.Controller.extend({
    default_planned_start_date: function() {
        if (this.frm.doc.default_planned_start_date){
            this._set_defaults('planned_start_date')
        }
    },
    default_planned_end_date: function() {
        if (this.frm.doc.default_planned_end_date){
            this._set_defaults('planned_end_date')
        }
    },
    default_warehouse: function() {
        if (this.frm.doc.default_warehouse){
            this._set_defaults('warehouse')
        }
    },    
    default_expected_delivery_date: function() {
        if (this.frm.doc.default_expected_delivery_date){
            this._set_defaults('expected_delivery_date')
        }
    },    
    _set_defaults: function(field_name) {
        for(let i =0; typeof this.frm.doc.items !== "undefined" && i < this.frm.doc.items.length && !this.frm.doc.items[i][field_name]; i++) {
            let row = frappe.get_doc(this.frm.doc.items[i].doctype, this.frm.doc.items[i].name)
            row[field_name] = this.frm.doc['default_' + field_name]
            erpnext.utils.copy_value_in_all_row(this.frm.doc, row.doctype, row.name, 'items', field_name)
            break
        }
    },
    item_code: function() {
        this.items_update()
    },
    items_update: function() {
        this.default_planned_start_date()
        this.default_planned_end_date()
        this.default_warehouse()
        this.default_expected_delivery_date()
    },
    get_items: function(){
        this.items_update()
    }
})

$.extend(cur_frm.cscript, new stephan.ProductionPlaningController({frm: cur_frm}))
