window.download_dialog = function (list) {
    var dialog;
    const filter_fields = df => frappe.model.is_value_type(df) && !df.hidden;
    const get_fields = dt => frappe.meta.get_docfields(dt).filter(filter_fields);

    const get_doctypes = parentdt => {
        return [parentdt].concat(
            frappe.meta.get_table_fields(parentdt).map(df => df.options)
        );
    };

    const get_doctype_checkbox_fields = () => {
        return dialog.fields.filter(df => df.fieldname.endsWith('_fields'))
            .map(df => dialog.fields_dict[df.fieldname]);
    }

    const doctype_fields = get_fields(list.doctype)
        .map(df => ({
            label: df.label,
            value: df.fieldname,
            checked: 1
        }));

    let fields = [
        {
            "label": __("Select Columns"),
            "fieldname": "select_columns",
            "fieldtype": "Select",
            "options": "All\nMandatory\nTemplate\nManually",
            "reqd": 1,
            "onchange": function () {
                const fields = get_doctype_checkbox_fields();
                fields.map(f => f.toggle(this.value === 'Manually'));
                dialog.fields_dict.template.toggle(this.value === 'Template')
            }
        },
        {
            "label": __("Template"),
            "fieldname": "template",
            "fieldtype": "Link",
            "options": "Download Template",
            "hidden": 1,
            "get_query": function(){
                return {
                    filters: {
                        ref_doctype:list.doctype
                    }
                }
            }
        },
        {
            "label": __("File Type"),
            "fieldname": "file_type",
            "fieldtype": "Select",
            "options": "Excel\nCSV",
            "default": "Excel"
        },
        {
            "label": list.doctype,
            "fieldname": "doctype_fields",
            "fieldtype": "MultiCheck",
            "options": doctype_fields,
            "columns": 2,
            "hidden": 1
        }
    ];

    const child_table_fields = frappe.meta.get_table_fields(list.doctype)
        .map(df => {
            return {
                "label": df.options,
                "fieldname": df.fieldname + '_fields',
                "fieldtype": "MultiCheck",
                "options": frappe.meta.get_docfields(df.options)
                    .filter(filter_fields)
                    .map(df => ({
                        label: df.label,
                        value: df.fieldname,
                        checked: 1
                    })),
                "columns": 2,
                "hidden": 1
            };
        });

    fields = fields.concat(child_table_fields);

    dialog = new frappe.ui.Dialog({
        title: __('Download Template'),
        fields: fields,
        primary_action: function (values) {
            var data = values;
            if (list.doctype) {
                var export_params = () => {
                    let columns = {};
                    if (values.select_columns === 'All') {
                        columns = get_doctypes(list.doctype).reduce((columns, doctype) => {
                            columns[doctype] = get_fields(doctype).map(df => df.fieldname);
                            return columns;
                        }, {});
                    } else if (values.select_columns === 'Mandatory') {
                        // only reqd child tables
                        const doctypes = [list.doctype].concat(
                            frappe.meta.get_table_fields(list.doctype)
                                .filter(df => df.reqd).map(df => df.options)
                        );

                        columns = doctypes.reduce((columns, doctype) => {
                            columns[doctype] = get_fields(doctype).filter(df => df.reqd).map(df => df.fieldname);
                            return columns;
                        }, {});
                    } else if (values.select_columns === 'Manually') {
                        columns = get_doctype_checkbox_fields().reduce((columns, field) => {
                            const options = field.get_checked_options();
                            columns[field.df.label] = options;
                            return columns;
                        }, {});
                    }
                    let check_items = $.map(list.get_checked_items(), (r) => {
                        return r.name
                    })
                    return {
                        doctype: list.doctype,
                        parent_doctype: list.doctype,
                        select_columns: JSON.stringify(columns),
                        download_template: values.template || "",
                        with_data: 'Yes',
                        all_doctypes: 'Yes',
                        from_data_import: 'Yes',
                        excel_format: data.file_type === 'Excel' ? 'Yes' : 'No',
                        filters: check_items.length ? [[list.doctype, "name", "in", check_items]]: []
                    };
                };
                let get_template_url = '/api/method/stephan.utils.exporter.get_template';
                open_url_post(get_template_url, export_params());
            } else {
                frappe.msgprint(__("Please select the Document Type."));
            }
            dialog.hide();
        },
        primary_action_label: __('Download')
    });

    return dialog;
};



$(document).on('page-change', function () {
    let routes = frappe.get_route()
    if (routes && routes[0] === "List") {
        const check_list_exists = setInterval(() => {
            console.log("Log")
            if (typeof cur_list.doctype !== "undefined") {
                cur_list.page.add_menu_item(__("Export"), () => {
                    window.download_dialog(cur_list).show()
                })
                clearInterval(check_list_exists)
            }
        })
    }
})

