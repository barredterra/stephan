frappe.provide('stephan')
cur_frm.add_fetch('rm_item_code', 'item_name', 'rm_item_name')
var add_summarized = 1;
stephan.PurchaseOrderController = erpnext.buying.PurchaseOrderController.extend({
   refresh: function(doc, cdt, cdn) {
		var me = this;
		// this._super();
		var allow_receipt = false;
		var is_drop_ship = false;

		for (var i in cur_frm.doc.items) {
			var item = cur_frm.doc.items[i];
			if(item.delivered_by_supplier !== 1) {
				allow_receipt = true;
			} else {
				is_drop_ship = true;
			}

			if(is_drop_ship && allow_receipt) {
				break;
			}
		}

		cur_frm.set_df_property("drop_ship", "hidden", !is_drop_ship);

       var doc1 = cur_frm.doc;

		if(doc1.docstatus == 1 && !in_list(["Closed", "Delivered"], doc1.status)) {
			if (this.frm.has_perm("submit")) {
				if(flt(doc.per_billed, 6) < 100 || flt(doc.per_received, 6) < 100) {
					cur_frm.add_custom_button(__('Close'), this.close_purchase_order, __("Status"));
				}
			}

			if(is_drop_ship && doc1.status!="Delivered"){
				cur_frm.add_custom_button(__('Delivered'),
					this.delivered_by_supplier, __("Status"));

				cur_frm.page.set_inner_btn_group_as_primary(__("Status"));
			}
		}

		else if(doc1.docstatus===0) {
			cur_frm.cscript.add_from_mappers();
		}

		if(doc1.docstatus == 1 && in_list(["Closed", "Delivered"], doc1.status)) {
			if (this.frm.has_perm("submit")) {
				cur_frm.add_custom_button(__('Re-open'), this.unclose_purchase_order, __("Status"));
			}
		}

		if(doc1.docstatus == 1 && doc1.status != "Closed") {
			if(flt(doc1.per_received, 2) < 100 && allow_receipt) {
				cur_frm.add_custom_button(__('Receipt'), this.make_purchase_receipt, __("Make"));

				if(doc1.is_subcontracted==="Yes") {
					cur_frm.add_custom_button(__('Material to Supplier'),
						function() { me.make_stock_entry(); }, __("Transfer"));
				}
			}

			if(flt(doc1.per_billed, 2) < 100)
				cur_frm.add_custom_button(__('Invoice'),
					this.make_purchase_invoice, __("Make"));

			if(flt(doc1.per_billed)==0 && doc.status != "Delivered") {
				cur_frm.add_custom_button(__('Payment'), cur_frm.cscript.make_payment_entry, __("Make"));
			}

			if(!doc1.auto_repeat) {
				cur_frm.add_custom_button(__('Subscription'), function() {
					erpnext.utils.make_subscription(doc1.doctype, doc1.name)
				}, __("Make"))
			}

			if(flt(doc1.per_billed)==0) {
				this.frm.add_custom_button(__('Payment Request'),
					function() { me.make_payment_request() }, __("Make"));
			}

			cur_frm.page.set_inner_btn_group_as_primary(__("Make"));
		}

		   if (document.querySelectorAll('[data-fieldname="wird_benötigt_für"]')[1] != undefined) {
            document.querySelectorAll('[data-fieldname="wird_benötigt_für"]')[1].style.height = '27px';
        }
	},
    onload: function(doc) {
        this._super()

          let $from_grid2 = $(this.frm.fields_dict.items.grid.form_grid.get(0))
        setTimeout(()=>{
            // $from_grid2.find('.grid-body > .grid-footer > .row > .grid-buttons > button').hide()
            $from_grid2.find('.grid-body > .grid-footer > .row > .text-right > .grid-upload').hide()
            $from_grid2.find('.grid-body > .grid-footer').show()
        }, 1000)

         let $from_grid = $(this.frm.fields_dict.supplied_items.grid.form_grid.get(0))
        setTimeout(()=>{
            // $from_grid.find('.grid-body > .grid-footer > .row > .grid-buttons > button').hide()
            // $from_grid.find('.grid-body > .grid-footer > .row > .text-right > .grid-upload').hide()
            $from_grid.find('.grid-body > .grid-footer').show()
        }, 1000)

        if (add_summarized == 1) {
            let btn = document.createElement('a');
            btn.innerText = 'Download Summarized Supplied Items';
            btn.className = 'grid-summarized btn btn-xs btn-default';
            this.frm.fields_dict.supplied_items.grid.wrapper.find('.grid-upload').removeClass('hide').parent().append(btn);
            btn.addEventListener("click", function () {

                frappe.call({
				method: "stephan.stephan.po3.download_table",
				args:{"docname":cur_frm.doc.name},
				freeze: true,
                                callback: function (r) {
				    console.log("DOWNLOAD BOMS AS PDF");
						console.log(r);
						if (r.message) {
							$(cur_frm.fields_dict['download_link'].wrapper).html("");
                            var _html = '';

                            for(var i=0;i<r.message.files.length;i++)
                            {
                                console.log(r.message.files[i].path);
                                _html +='<a id="downloadleadcsv'+i.toString()+'" ' +
                                    'href="'+r.message.files[i].path+'" download > jpg link </a>' +
										'<script type="text/javascript">' +
                                    'document.getElementById("downloadleadcsv'+i.toString()+'").click(); </script>'

                            }

                            console.log(_html);

							 if (cur_frm.fields_dict['download_link']) {
                            		$(cur_frm.fields_dict['download_link'].wrapper)
                                        .html(_html);

							 }
						}
					}
			})

            });


               let btn2 = document.createElement('a');
            btn2.innerText = 'Download Supplied Items';
            btn2.className = 'grid-supplied btn btn-xs btn-default';
            this.frm.fields_dict.supplied_items.grid.wrapper.find('.grid-summarized').removeClass('hide').parent().append(btn2);
            btn2.addEventListener("click", function () {

                       frappe.call({
				method: "stephan.stephan.po.download_table",
				args:{"docname":cur_frm.doc.name},
				freeze: true,
                                       callback: function (r) {
				    console.log("DOWNLOAD BOMS AS PDF");
						console.log(r);
						if (r.message) {
							$(cur_frm.fields_dict['download_link'].wrapper).html("");
                            var _html = '';

                            for(var i=0;i<r.message.files.length;i++)
                            {
                                console.log(r.message.files[i].path);
                                _html +='<a id="downloadleadcsv'+i.toString()+'" ' +
                                    'href="'+r.message.files[i].path+'" download > jpg link </a>' +
										'<script type="text/javascript">' +
                                    'document.getElementById("downloadleadcsv'+i.toString()+'").click(); </script>'

                            }

                            console.log(_html);

							 if (cur_frm.fields_dict['download_link']) {
                            		$(cur_frm.fields_dict['download_link'].wrapper)
                                        .html(_html);

							 }
						}
					}
			});

            });

            add_summarized = 0;
        }
    },
    default_warehouse: function (doc) {
        this.set_default_warehouse_for_missing();
    },
    overwrite_default_warehouse: function(doc) {
        this.set_default_warehouse_for_missing(doc)
    },
    set_default_warehouse_for_missing: function() {
        let me = this;
        if (!me.frm.doc.default_warehouse)
            return;
        let items = this.frm.doc.items || []
        items.forEach(function(row){
            if (!row.warehouse || me.frm.doc.overwrite_default_warehouse) {
                row.warehouse = me.frm.doc.default_warehouse
            }
        })
        this.frm.refresh_field('items')
    },
    validate: function(doc, cdt, cdn) {
        this._super(doc, cdt, cdn);
        this.set_missing_item_name()
    },
    item_code: function(doc, cdt, cdn) {
        let me = this;
        let row = frappe.get_doc(cdt, cdn)
        this._super(doc, cdt, cdn).then(()=>{
            if (!row.warehouse && me.frm.doc.default_warehouse) {
                row.warehouse = me.frm.doc.default_warehouse
            }
        })
        this.frm.refresh_field('items')
    },
    default_reserve_warehouse: function(doc) {
        this.set_reverse_warehouse(doc)
    },
    overwrite_reserve_warehouse: function(doc) {
        this.set_reverse_warehouse(doc)
    },
    set_reverse_warehouse: function(doc) {
        if (!doc.default_reserve_warehouse)
            return;
        if (doc.supplied_items) {
            doc.supplied_items.forEach(row=>{
                if (doc.overwrite_reserve_warehouse || !row.reserve_warehouse)
                    row.reserve_warehouse = doc.default_reserve_warehouse
            })
            this.frm.refresh_field('supplied_items')
        }
    },
    set_missing_item_name: function() {
        if (this.frm.doc.supplied_items) {
            this.frm.doc.supplied_items.forEach(row=>{
                if(!row.rm_item_name)
                    frappe.db.get_value("Item", {"name": row.rm_item_code}, "item_name", (r)=> {
                        row.rm_item_name = r.item_name
                    });
            })
        }
    }
})


$.extend(cur_frm.cscript, new stephan.PurchaseOrderController({frm: cur_frm}))
frappe.ui.form.on("Purchase Order", {
    download_as_pdf:function (frm) {


       frappe.call({
				method: "stephan.stephan.make_pdf.make_pdf_boms",
				args:{"docname":cur_frm.doc.name,"doctype":"Purchase Order"},
				freeze: true,
                callback: function (r) {
				    console.log("DOWNLOAD BOMS AS PDF");
						console.log(r);
						if (r.message) {
							$(frm.fields_dict['download_link'].wrapper).html("");
                            var _html = '';

                            for(var i=0;i<r.message.files.length;i++)
                            {
                                console.log(r.message.files[i].path);
                                _html +='<a id="downloadleadcsv'+i.toString()+'" ' +
                                    'href="'+r.message.files[i].path+'" download > jpg link </a>' +
										'<script type="text/javascript">' +
                                    'document.getElementById("downloadleadcsv'+i.toString()+'").click(); </script>'

                            }

                            console.log(_html);

							 if (frm.fields_dict['download_link']) {
                            		$(frm.fields_dict['download_link'].wrapper)
                                        .html(_html);

							 }
						}
					}
			})
    },
	setup: function(frm) {
        frm.set_indicator_formatter('item_code',
			function(doc) { 
            return (doc.qty<=doc.received_qty) ? "green" : "orange"
        }, function(doc){
            let formatted_val = "";
            if (doc.image)
                formatted_val += `<span class="avatar avatar-small"><span class="avatar-frame" style="background-image: url(${doc.image})"></span></span>`;
            if (doc.item_code===doc.item_name){

                formatted_val += `${doc.item_code}`
            }
            else {
                formatted_val += `${doc.item_code}:${doc.item_name}`
            }
            return formatted_val
        })
        // Set page container as fluid 
        $($(frm.parent).find(' > .page-body').get(0)).addClass('container-fluid').removeClass('container')

		if (frm.doc.docstatus==1) {
            setTimeout(() => {
                frm.add_custom_button(__("Stock Entry"), function(){
                    console.log('Hi')
                    frm.events.make_manual_stock_entry()
                }, __("Make"))
            }, 500);
		}
    },

	make_manual_stock_entry: function() {
		frappe.model.open_mapped_doc({
			method: "stephan.utils.purchase_order.make_manual_stock_entry",
			frm: cur_frm
		})
	},
    // download_supplied_items:function(){
	 //    console.log("hello items");
    //     frappe.call({
		// 		method: "stephan.stephan.po.queue_generate_spreadsheet",
		// 		args:{"docname":cur_frm.doc.name},
		// 		freeze: true
		// 	})
    // }
    // ,
    download_as_excel:function(frm){
	    console.log("hello items");
        frappe.call({
				method: "stephan.stephan.po2.download_table",
				args:{"docname":cur_frm.doc.name},
				freeze: true,
                callback: function (r) {
				    console.log("DOWNLOAD BOMS AS PDF");
						console.log(r);
						if (r.message) {
							$(frm.fields_dict['download_link'].wrapper).html("");
                            var _html = '';

                            for(var i=0;i<r.message.files.length;i++)
                            {
                                console.log(r.message.files[i].path);
                                _html +='<a id="downloadleadcsv'+i.toString()+'" ' +
                                    'href="'+r.message.files[i].path+'" download > jpg link </a>' +
										'<script type="text/javascript">' +
                                    'document.getElementById("downloadleadcsv'+i.toString()+'").click(); </script>'

                            }

                            console.log(_html);

							 if (frm.fields_dict['download_link']) {
                            		$(frm.fields_dict['download_link'].wrapper)
                                        .html(_html);

							 }
						}
					}
			});
    }
})