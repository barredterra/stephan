frappe.listview_settings['BOM'] = {
    onload: function(list) {

        list.page.add_menu_item(__("Update VR"), function() {
			// console.log(list.get_checked_items(true));
            var items = list.get_checked_items(true);
            for(var i=0;i<items.length;i++) {
                frappe.call({
                    method: "stephan.stephan.bom.update_cost_",
                    freeze: true,
                    args: {
                        docname: items[i],
                        update_parent: true,
                        from_child_bom: false,
                        save: false
                    },
                    callback: function (r) {
                        // refresh_field("items");
                        // if (!r.exc) frm.refresh_fields();
                        // location.reload();
                        console.log("done update cost.");
                    }
                });
            }
		});

         list.page.add_menu_item(__("Update Images"), function() {
			// console.log(list.get_checked_items(true));
            var items = list.get_checked_items(true);
            for(var i=0;i<items.length;i++) {
                  frappe.call({
                        method: "stephan.utils.bom.update_item",
                        args: {
                            bom: items[i],
                            fields: {'image':'image'}
                        },
                        freeze: true,
                        callback: r => {
                            if(r['message']){
                                console.log("done update image.");
                            }
                        }
                    })
            }
		});

         list.page.add_menu_item(__("Update Item Name"), function() {
			// console.log(list.get_checked_items(true));
            var items = list.get_checked_items(true);
            for(var i=0;i<items.length;i++) {
                  frappe.call({
                        method: "stephan.utils.bom.update_item",
                        args: {
                            bom: items[i],
                            fields: {'item_name':'item_name'}
                        },
                        freeze: true,
                        callback: r => {
                            if(r['message']){
                                console.log("done update image.");
                            }
                        }
                    })
            }
		});





        list.meta.title_field = 'name'
        list.get_subject_html = function(doc) {
            let user = frappe.session.user;
            let subject_field = this.columns[0].df;
            let value = doc[subject_field.fieldname] || doc.name;
            if (doc.image) {
                var image = (window.cordova && doc.image.indexOf('http')===-1) ?
                frappe.base_url + doc.image : doc.image;
    
                value = repl('<span class="avatar avatar-small" title="%(title)s">\
                    <span class="avatar-frame" style="background-image: url(%(image)s)"\
                    title="%(title)s"></span></span> %(title)s', {
                        image: image,
                        title: value
                    });
            }
            let subject = strip_html(value);
            let escaped_subject = frappe.utils.escape_html(subject);
    
            const liked_by = JSON.parse(doc._liked_by || '[]');
            let heart_class = liked_by.includes(user) ?
                'liked-by' : 'text-extra-muted not-liked';
    
            const seen = JSON.parse(doc._seen || '[]')
                .includes(user) ? '' : 'bold';
    
            let subject_html = `
                <input class="level-item list-row-checkbox hidden-xs" type="checkbox" data-name="${doc.name}">
                <span class="level-item" style="margin-bottom: 1px;">
                    <i class="octicon octicon-heart like-action ${heart_class}"
                        data-name="${doc.name}" data-doctype="${this.doctype}"
                        data-liked-by="${encodeURI(doc._liked_by) || '[]'}"
                    >
                    </i>
                    <span class="likes-count">
                        ${ liked_by.length > 99 ? __("99") + '+' : __(liked_by.length || '')}
                    </span>
                </span>
                <span class="level-item ${seen} ellipsis" title="${escaped_subject}">
                    <a class="ellipsis" href="${this.get_form_link(doc)}" title="${escaped_subject}">
                    ${value}
                    </a>
                </span>
            `;
    
            return subject_html;
        }
        // S
        let status_idx;
        let subject_idx;
        let is_active_df;
        let sup;
        list.meta.fields.forEach((row) => {
            if (row.fieldname === "is_active"){
                is_active_df = row
            } else if (row.fieldname === "default_supplier"){
                sup = row
            }
        });
        list.columns.forEach((row, idx) => {
            if (row.type==="Status")
                status_idx = idx;
            else if (row.type==="Subject")
                subject_idx = idx;

        });
        // list.columns.push({
        //     colspan: 2,
        //     content: sup.fieldname,
        //     fieldname: sup.fieldname,
        //     fieldtype: sup.fieldtype,
        //     df: sup,
        //     title: sup.label,
        //     type: sup.fieldtype
        // })
        list.columns[subject_idx].colspan = 9
        list.columns[status_idx]['df'] = {
                label: __("Is Active")
            }
        list.$result.find('.list-row-head').remove()
        list.render_header()
        console.log(list.columns[status_idx])
        // list.columns.splice(status_idx,1, {
        //     fieldname: "is_active",
        //     fieldtype: "Check",
        //     title: __("Is Active"),
        //     type: "Indicator",
        //     df: is_active_df,
        //     colspan: 1,
        //     content: "is_active"
        // })
    },
    refresh: function (list) {
        list.$list_head_subject.children('.list-subject').css('flex-grow', 3)
    },
    has_indicator_for_draft: true,
    get_indicator: function(doc) {
		if (doc.is_active) {
			return [__("Active"), "green", "is_active,=,Yes"];
		} else{
			return [__("Inactive"), "grey", "is_active,=,Yes"];
		}
	}
}