frappe.form.link_formatters['Item'] = function(value, doc) {
    let val = value
    if(doc.item_name && doc.item_name !== value) {
        val += ': ' + doc.item_name;
    }
    if (doc.image)
        val = `<span class="avatar avatar-small"> <span class="avatar-frame" style="background-image: url(${doc.image})"></span></span> ${val}`;
    return val
}