/**
 * Created by jvfiel on 11/6/16.
 */
frappe.listview_settings['Stock Entry'] = {

    onload:function(listview){

        if( listview.filter_list)
        {listview.filter_list.clear_filters();}

        var me =listview;

         me.page.add_sidebar_item(__("Submitted"), function () {
            var assign_filter = me.filter_area.filter_list.get_filter("docstatus");
            assign_filter && assign_filter.remove(true);

            me.filter_area.filter_list.add_filter(me.doctype, "docstatus", '=', "1");
             me.refresh();
              // console.log(me);
        });


          me.page.add_sidebar_item(__("Draft"), function () {
            var assign_filter = me.filter_area.filter_list.get_filter("docstatus");
            assign_filter && assign_filter.remove(true);

            me.filter_area.filter_list.add_filter(me.doctype, "docstatus", '=', "0");
             me.refresh();
              // console.log(me);
        });

          me.page.add_sidebar_item(__("Cancelled"), function () {
            var assign_filter = me.filter_area.filter_list.get_filter("docstatus");
            assign_filter && assign_filter.remove(true);

            me.filter_area.filter_list.add_filter(me.doctype, "docstatus", '=', "2");
             me.refresh();
              // console.log(me);
        });



    }
};